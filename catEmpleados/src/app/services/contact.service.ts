import { Injectable } from '@angular/core';
import { Contact } from '../interfaces/Contact';
import { HttpClient, HttpHeaders, HttpClientModule} from'@angular/common/http';
import { Observable } from 'rxjs';
import { map } from "rxjs/operators"

@Injectable({
  providedIn: 'root'
})
export class ContactService {

  // Contactos almacenados
  private _listContacts: Contact[];

  // Recuerda que el constructor de los servicios solo se ejecutan una vez
  constructor(private httpClient: HttpClient) {
    this._listContacts = [];
  }

  /**
   * Devuelve la lista de contactos
   */
  get listContacts(): Contact[]{
    return this._listContacts;
  }


  /**
   * Recoge los contactos del fichero json
   * @param path ruta del fichero json
   */
  getData(path: string){
    return this.httpClient.get<Contact[]>(path).pipe(
      map(list => {
        this._listContacts = list;
        return list;
      })
    );
  }

  /**
   * Obtiene el listado de los contactos desde el servicio
   */
  getContactos(): Observable<any>{
    return this.httpClient.get("http://localhost:8071/readAllData");

  }

   /**
   * Inserta un contacto 
   */
  addContact(contacto:Contact): Observable<any>{
    
    let json = JSON.stringify(contacto);
    let headers = new HttpHeaders().set('Content-type', 'application/json');

    return this.httpClient.post("http://localhost:8071/createData/", json, {headers:headers});
  }

  /**
   * Elimina un contacto 
   */
  delContact(id:string): Observable<any>{
  
    return this.httpClient.delete("http://localhost:8071/deleteData?id="+id);
  }

 /**
   * Elimina todos los contactos
   */
  dellAllContacts():Observable<any>{
    return this.httpClient.delete("http://localhost:8071/deleteAllData");
  }
}
