export interface Contact {
    id: string,
    data:{
        nombre: string,
        apellidoPat: string,
        apellidoMat: string,     
        email: string        
    }
}