import { Contact } from './../../../interfaces/Contact';
import { ContactService } from './../../../services/contact.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-list-contacts',
  templateUrl: './list-contacts.component.html',
  styleUrls: ['./list-contacts.component.css']
})
export class ListContactsComponent implements OnInit {

  // Atributos
  public listContacts: Contact[];
  private route: Router;

  // Inyecto el servicio de ContactService
  constructor(
    private contactService: ContactService
  ) {
    this.listContacts = [];
  }

  ngOnInit() {
    
      // obtenemos los contactos
      this.contactService.getContactos().subscribe(list => {
        this.listContacts = list;
      });
    console.log(this.contactService.listContacts);

  }

  /**
   * Elimina contacto
   */
  delContact(id:any) {

    console.log(id);
    // Eliminar el contacto
    console.log("evento eliminar")
     this.contactService.delContact(id.id).subscribe(resultado =>{
     this.contactService.getContactos();
    },
    error => {
      console.log(JSON.stringify(error));
    });

    //recuperamos del servicio
    this.ngOnInit() ;
  }

}
