import { ContactService } from '../../services/contact.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: 'app-schedule',
  templateUrl: './schedule.component.html',
  styleUrls: ['./schedule.component.css']
})
export class ScheduleComponent implements OnInit {

  constructor( private contactService: ContactService,
    private route: Router) { }

  ngOnInit() {
  }

   /**
   * Elimina contactos
   */
  delAll() {
    // Eliminar contactos
    console.log("evento eliminar todos")
     this.contactService.dellAllContacts().subscribe(resultado =>{
    },
    error => {
      console.log(JSON.stringify(error));
    });

      // Vuelvo al inicio
      this.route.navigate(['/list-contacts/list-contact']);
    
  }

}
