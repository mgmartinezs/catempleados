import { ContactService } from './../../../services/contact.service';
import { Contact } from './../../../interfaces/Contact';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-contact',
  templateUrl: './add-contact.component.html',
  styleUrls: ['./add-contact.component.css']
})
export class AddContactComponent implements OnInit {

  // Atributos
  public contact: Contact;

  // Route sirve para manejar rutas
  constructor(
    private contactService: ContactService,
    private route: Router
  ) {
    this.contact = {
      'id': '',
      'data':{
          'nombre': '',
          'apellidoPat': '',
          'apellidoMat': '',     
          'email': ''        
      }
    }
  }

  ngOnInit() {
  }

  /**
   * Añade el contacto, con los datos añadidos en el formulario
   */
  addContact() {

    console.log(this.contact);
    // Añado el contacto
    console.log("evento agregar")

    this.contactService.addContact(this.contact).subscribe(resultado =>{
      
    }, 
    error=>{
      console.log(JSON.stringify(error));
    });

    // Vuelvo al inicio
    this.route.navigate(['/list-contact']);
  }

}
